# AmbientZzz-Pi by dusti-alle-95
------------------------------------------
Script to play ambient sounds on startup on a raspberry pi 3 with raspbian lite

## From scratch Rapsberry Pi installation instructions 
1. Install raspbain lite
2. Configure to your liking (enable ssh, etc)
3. Enable automatic login option using raspi-config

## AmbientZzz-Pi installation instructions
4. `sudo apt install libavcodec-extra omxplayer`
5. Download and extract the AmbientZzz-Pi zip to your home directory 
    * Alternatively, use `git clone https://gitlab.com/dusti-alle-95/AmbientZzz-Pi.git` in your home direcotry
6. Place desired ambient audio files in your audio folder (~/AmbientZzz-Pi/audio/ by default)
7. Configure to your liking using config.txt
8. Run ambient.sh to initiate playback of soothing sounds
9. (Optional) Configure cron jobs to run ambient.sh on startup to use your pi as a dedicated sleep aid.

AmbientZzz-Pi does assume it is installed in the home directory of the "pi" user, if this is not the case the location of the base directory can be easily modified within ambient.sh

AmbientZzz-Pi comes with an example audio file in the default audio directory for demonstration purposes, feel free to delete or move it.
## Configuration:
Configuration can be done via modifying config.txt.
Listed below are the configurable options and the values they may take.

    playback="list"  		( random | list | "FILENAME.EXTENSION" )		 Audio playback order, random randomly selects a file from the audio directory, list iterates through each file in the audio directory, entering a specfic filename will attempt to playback only that specific file within the audio directory (e.g. x.opus is entered in for audio file named x.opus that is placed within the audio directory) 
    audioPath='/home/pi/AmbientZzz-Pi/audio'		( "/path/to/audio/" )    	Location of the audio files used for playback, all audio files must be contained within the root of this directory 
    output='local' 			( hdmi | local | both )			Configures which output(s) of the Raspberry Pi are used in omxplayer
	loop='true'			( true | false )			Boolean that controls whether the played back audio file loops or not


## Example cron jobs
Run on startup and shutdown after 3 hours:

    crontab -e
    @reboot /home/pi/AmbientZzz-Pi/ambient.sh
    sudo crontab -e
    @reboot shutdown -P 300
    
Run on startup and end execution after 3 hours 

    crontab -e
    @reboot timeout 10800 /home/pi/AmbientZzz-Pi/ambient.sh

### [Demo Video](https://gitlab.com/dusti-alle-95/AmbientZzz-Pi/raw/video/demo/demo.mp4)


### Credits:
Failure 01 by rhodesmas : https://freesound.org/people/rhodesmas/sounds/342756/
ventilation-fan-drone by yawfle : https://freesound.org/people/yawfle/sounds/19849/
https://creativecommons.org/licenses/by/3.0/


