#!/bin/bash
#script to play an audio file indefintly from the ~ambient/audio directory, meant for raspberry pi ambient/sleep aid audio playback

#load variables from config
ambientPath="/home/pi/AmbientZzz-Pi/"
audioList='audioFileList.tmp'
playbackList='playbackList.txt'
failure='failure.wav'
config='config.txt'
source $ambientPath$config


#functions
exit_ambient(){
	if [ -e $ambientPath$audioList ]
	then
		rm $ambientPath$audioList
	fi
	if [[ (! -z "$1") && ($1 == "failure") ]]
	then
		omxplayer -o $output  $ambientPath$failure
	fi
	exit 1
}

#verify configuration
if [ ! -e $ambientPath ]
	then
		echo "ambientPath variable improperly defined!"
		exit_ambient failure
fi

if [ ! -e $audioPath ]
	then
		echo "Config.txt configuration error! Audio path incorrect"
		exit_ambient failure
fi

if [ "$output" != "hdmi" ] && [ "$output" != "local" ] && [ "$output" != "both" ]
	then
		echo "Config.txt configuration error! Output incorrect"
		exit_ambient failure
fi

if [ "$loop" == "true" ]
	then
		omxloop='--loop'
elif [ "$loop" == "false" ]
	then
		omxloop=''
else
	echo "Config.txt configuration error! loop value incorrect"
	exit_ambient failure	
fi

#get list of audio files
find $audioPath -maxdepth 1 -type f > $ambientPath$audioList


if [ "$playback" == "random" ]
then
	echo "----- Random playback mode -----"
	audioname=$(shuf -n 1 "$ambientPath$audioList")
	echo "----- Now playing $audioname  -----"
	omxplayer -o $output $omxloop $audioname 
	exit_ambient
elif [ "$playback" == "list" ]
then
	echo "----- Set order playback mode -----"
	
	if [ -f $ambientPath$playbackList ]
	then
		tail -n +2 $ambientPath$playbackList | tee $ambientPath$playbackList > /dev/null 
	fi

	if [[ (! -f $ambientPath$playbackList) || (! -s $ambientPath$playbackList) ]]
	then
		cp $ambientPath$audioList $ambientPath$playbackList		
	fi

	currentPlayback=$(head -n 1 $ambientPath$playbackList)
	echo "----- Now playing $currentPlayback -----"
	omxplayer -o $output $omxloop $currentPlayback
	exit_ambient
else
	if [ ! -f $audioPath\/$playback ] 
		then    
		echo "File not found, exiting script..."
		exit_ambient failure
	else
		echo "----- Specified file playback mode -----"
		echo "----- Now playing $playback -----"
		omxplayer -o $output $omxloop $audioPath\/$playback
		exit_ambient
	fi
fi

echo "FATAL ERROR"
exit_ambient failure


